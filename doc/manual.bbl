\newcommand{\SortNoOp}[1]{}
\begin{thebibliography}{17}
\expandafter\ifx\csname natexlab\endcsname\relax\def\natexlab#1{#1}\fi

\bibitem[Misquitta and Stone(2023)]{CamCASP7.0}
A.~J. Misquitta and A.~J. Stone, {CamCASP}: a program for studying
  intermolecular interactions and for the calculation of molecular properties
  in distributed form, University of Cambridge,  (2023),
  https://github.com/ajmisquitta/camcasp-bin.

\bibitem[Smith \emph{et~al.}(2020)Smith, Burns, Simmonett
  \emph{et~al.}]{Psi4-1.4}
D.~G.~A. Smith, L.~A. Burns, A.~C. Simmonett \emph{et~al.}, J.~Chem. Phys. 152
  (2020) 184108.

\bibitem[Frisch \emph{et~al.}(2016)Frisch, Trucks, Schlegel, Scuseria, Robb,
  Cheeseman, Scalmani, Barone, Petersson, Nakatsuji, Li, Caricato, Marenich,
  Bloino, Janesko, Gomperts, Mennucci, Hratchian, Ortiz, Izmaylov, Sonnenberg,
  Williams-Young, Ding, Lipparini, Egidi, Goings, Peng, Petrone, Henderson,
  Ranasinghe, Zakrzewski, Gao, Rega, Zheng, Liang, Hada, Ehara, Toyota, Fukuda,
  Hasegawa, Ishida, Nakajima, Honda, Kitao, Nakai, Vreven, Throssell,
  Montgomery, Peralta, Ogliaro, Bearpark, Heyd, Brothers, Kudin, Staroverov,
  Keith, Kobayashi, Normand, Raghavachari, Rendell, Burant, Iyengar, Tomasi,
  Cossi, Millam, Klene, Adamo, Cammi, Ochterski, Martin, Morokuma, Farkas,
  Foresman and Fox]{Gaussian16}
M.~J. Frisch, G.~W. Trucks, H.~B. Schlegel, G.~E. Scuseria, M.~A. Robb, J.~R.
  Cheeseman, G.~Scalmani, V.~Barone, G.~A. Petersson, H.~Nakatsuji, X.~Li,
  M.~Caricato, A.~V. Marenich, J.~Bloino, B.~G. Janesko, R.~Gomperts,
  B.~Mennucci, H.~P. Hratchian, J.~V. Ortiz, A.~F. Izmaylov, J.~L. Sonnenberg,
  D.~Williams-Young, F.~Ding, F.~Lipparini, F.~Egidi, J.~Goings, B.~Peng,
  A.~Petrone, T.~Henderson, D.~Ranasinghe, V.~G. Zakrzewski, J.~Gao, N.~Rega,
  G.~Zheng, W.~Liang, M.~Hada, M.~Ehara, K.~Toyota, R.~Fukuda, J.~Hasegawa,
  M.~Ishida, T.~Nakajima, Y.~Honda, O.~Kitao, H.~Nakai, T.~Vreven,
  K.~Throssell, J.~A. Montgomery, {Jr.}, J.~E. Peralta, F.~Ogliaro, M.~J.
  Bearpark, J.~J. Heyd, E.~N. Brothers, K.~N. Kudin, V.~N. Staroverov, T.~A.
  Keith, R.~Kobayashi, J.~Normand, K.~Raghavachari, A.~P. Rendell, J.~C.
  Burant, S.~S. Iyengar, J.~Tomasi, M.~Cossi, J.~M. Millam, M.~Klene, C.~Adamo,
  R.~Cammi, J.~W. Ochterski, R.~L. Martin, K.~Morokuma, O.~Farkas, J.~B.
  Foresman and D.~J. Fox  (2016), Gaussian Inc. Wallingford CT.

\bibitem[Stone(1981)]{Stone81a}
A.~J. Stone, Chem. Phys. Lett. 83 (1981) 233.

\bibitem[Stone and Alderton(1985)]{StoneA85}
A.~J. Stone and M.~Alderton, Molec. Phys. 56 (1985) 1047.

\bibitem[Stone(2013)]{timf}
A.~J. Stone, The Theory of Intermolecular Forces, (Oxford University Press,
  Oxford, 2013), 2nd edn.

\bibitem[Boys(1950)]{Boys50}
S.~F. Boys, Proc. Roy. Soc. A 200 (1950) 542.

\bibitem[Stone(2005)]{Stone05b}
A.~J. Stone, J.~Chem. Theory Comput. 1 (2005) 1128.

\bibitem[Lebedev and Laikov(1999)]{LebedevL99}
V.~I. Lebedev and D.~N. Laikov, Doklady Mathematics 59 (1999) 477.

\bibitem[Becke(1988)]{Becke88}
A.~D. Becke, J.~Chem. Phys. 88 (1988) 2547.

\bibitem[Murray \emph{et~al.}(1993)Murray, Handy and Laming]{MurrayHL93}
C.~W. Murray, N.~C. Handy and G.~J. Laming, Molec. Phys. 78 (1993) 997.

\bibitem[Slater(1964)]{Slater64}
J.~C. Slater, J.~Chem. Phys. 41 (1964) 3199.

\bibitem[Stone \emph{et~al.}(2018)Stone, Dullweber, Engkvist, Fraschini,
  Hodges, Meredith, Nutt, Popelier and Wales]{Orient5.0}
A.~J. Stone, A.~Dullweber, O.~Engkvist, E.~Fraschini, M.~P. Hodges, A.~W.
  Meredith, D.~R. Nutt, P.~L.~A. Popelier and D.~J. Wales, ORIENT: a program
  for studying interactions between molecules, version 5.0,  (2018),
  https://gitlab.com/anthonyjs/orient.git.

\bibitem[Stone \emph{et~al.}(2012)Stone, Dullweber, Engkvist, Fraschini,
  Hodges, Meredith, Nutt, Popelier and Wales]{Orient4.8}
A.~J. Stone, A.~Dullweber, O.~Engkvist, E.~Fraschini, M.~P. Hodges, A.~W.
  Meredith, D.~R. Nutt, P.~L.~A. Popelier and D.~J. Wales, ORIENT: a program
  for studying interactions between molecules, version 4.8, University of
  Cambridge,  (2012), http://www-stone.ch.cam.ac.uk/programs.html\#Orient.

\bibitem[Winn \emph{et~al.}(1997)Winn, Ferenczy and Reynolds]{WinnFR97}
P.~J. Winn, G.~G. Ferenczy and C.~A. Reynolds, J.~Phys. Chem. A 101 (1997)
  5437.

\bibitem[Ferenczy \emph{et~al.}(1997)Ferenczy, Winn and Reynolds]{FerenczyWR97}
G.~G. Ferenczy, P.~J. Winn and C.~A. Reynolds, J.~Phys. Chem. A 101 (1997)
  5446.

\bibitem[Misquitta \emph{et~al.}(2014)Misquitta, Stone and
  Fazeli]{MisquittaSF14}
A.~J. Misquitta, A.~J. Stone and F.~Fazeli, J.~Chem. Theory Comput. 10 (2014)
  5405.

\end{thebibliography}
